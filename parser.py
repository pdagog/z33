# This file contain the parser for the z33 assembly.
# The parser analyses a given string and return a tuple with the following format:
#
# If the string represents an instruction:
# result = ('instr', str, cmd, op)
# where str is the string as it was given to the parser, cmd represents
# the instruction, and op is a list of operand where each operand is
# tuple containing the addressing mode and their parameters.
#
# If the string represents data
# result = ('data', nb)7
# where nb is the numerical data.
#
# Examples:
# 
# The parsing of 'ld [%sp+1],%a' will return the following tuple
# ('instr', 'ld [%sp+1],%a', 'ld', [('idx', 'sp', 1), ('reg', 'a')])
# 
# The parsing of 'CMP 0x1,%A' will return the following tuple
# ('instr', 'CMP 0x1, %a', 'cmp', [('imm', 1), ('reg', 'a')])
# 
# The numerical data 42 will be converted into 
# ('data', 42)


reserved = {
    'add': 'ADD',
    'and': 'AND',
    'call': 'CALL',
    'cmp': 'CMP',
    'div': 'DIV',
    'fas': 'FAS',
    'in': 'IN',
    'jeq': 'JEQ',
    'jge': 'JGE',
    'jgt': 'JGT',
    'jle': 'JLE',
    'jlt': 'JLT',
    'jmp': 'JMP',
    'jne': 'JNE',
    'ld': 'LD',
    'mul': 'MUL',
    'neg': 'NEG',
    'nop': 'NOP',
    'not': 'NOT',
    'or': 'OR',
    'out': 'OUT',
    'pop': 'POP',
    'push': 'PUSH',
    'reset': 'RESET',
    'rti': 'RTI',
    'rtn': 'RTN',
    'shl': 'SHL',
    'shr': 'SHR',
    'st': 'ST',
    'sub': 'SUB',
    'swap': 'SWAP',
    'trap': 'TRAP',
    'xor': 'XOR',
}

tokens = [
    'CPP',
    'COMMENT1',
    'IDENT',
    'STRING',
    'COMMA', 'NEWLINE',
    'COLON', 'SEMICOLON',
    'REG', 'NUMBER',
    'LPAREN', 'RPAREN',
    'LBRACKET', 'RBRACKET',
    'PLUS', 'MINUS'
] + list(reserved.values())

# Tokens

t_ADD = r'add'
t_AND = r'and'
t_CALL = r'call'
t_CMP = r'cmp'
t_DIV = r'div'
t_FAS = r'fas'
t_IN = r'in'
t_JEQ = r'jeq'
t_JGE = r'jge'
t_JGT = r'jgt'
t_JLE = r'jle'
t_JLT = r'jlt'
t_JMP = r'jmp'
t_JNE = r'jne'
t_LD = r'ld'
t_MUL = r'mul'
t_NEG = r'neg'
t_NOP = r'nop'
t_NOT = r'not'
t_OR = r'or'
t_OUT = r'out'
t_POP = r'pop'
t_PUSH = r'push'
t_RESET = r'reset'
t_RTI = r'rti'
t_RTN = r'rtn'
t_SHL = r'shl'
t_SHR = r'shr'
t_ST = r'st'
t_SUB = r'sub'
t_SWAP = r'swap'
t_TRAP = r'trap'
t_XOR = r'xor'

t_COMMA = r','
t_SEMICOLON = r';'
t_COLON = r':'
t_PLUS = r'\+'
t_MINUS   = r'-'
#t_TIMES   = r'\*'
#t_DIVIDE  = r'/'
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
t_LBRACKET  = r'\['
t_RBRACKET  = r'\]'


import ply.lex as lex

# LexToken has attributes :
#     tok.type,
#     tok.value,
#     tok.lineno
#     tok.lexpos  = index of the token relative to the start of the input text


# A string containing ignored characters (spaces and tabs)
t_ignore = ' \t'

def t_CPP(t):                                   # cpp directives
    r'(?m)^\#[^\n]*' # (?m)^ states for each start of line while ^ is for start of string
    # TODO traiter
    pass  # No return value -> token discarded

def t_NUMBER(t):
    #     hexadecimal        decimal     octal
    r'(0[xX][0-9a-fA-F]+)|([1-9][0-9]*)|(0[0-7]*)'   # warning: order is important
    try:
        if t.value[0:2].lower() == '0x':
            t.value = int(t.value[2:], base=16)
        elif t.value[0] == '0':
            t.value = int(t.value, base=8)
        else:
            t.value = int(t.value, base=10)
    except ValueError:
        print("Invalid value '{}' in line {}".format(t.value, t.lineno))
        t.value = 0
    return t

def t_STRING(t):
    r'"[^"]*"'                                  # TODO process \
    return t

def t_REG(t):
    r'%[a-zA-Z][_0-9a-zA-Z]*'                   # registers
    t.value = t.value.lower()[1:]
    return t

def t_IDENT(t):                                 # labels
    r'[a-zA-Z][_0-9a-zA-Z]*'
    t.type = reserved.get(t.value, 'IDENT')     # Check for reserved words
    return t

def t_COMMENT1(t):                              # //
    r'//[^\n]*'
    # TODO recognize /* ... */ comments
    pass

# Usefull to track line numbers
def t_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    return t

# Error handling rule
def t_error(t):
    print("Illegal character '{}' in line {}".format(t.value[0], t.lineno))
    t.lexer.skip(1)  # Skip ahead 1 char

# Build the lexer
lexer = lex.lex()

# POUR VOIR LES LEXEMES
# lexer.input ('abc : add (%ab + 3) , 0x10aF , 0377 , 456 // un commentaire\n\nsub %a,abc')
# for tok in lexer:
#    print(tok)

import ply.yacc as yacc

VAL_UNRESOLVED = '__UNRESOLVED__'
LABELS = {}
INSTR = []
CUR_ADDR = 0#INIT_ADDR
UNRESOLVED = {}


def p_program(p):
    'program : program line'
    instr = (p[1][1])[:]            # list copy
    # Ignore blank line
    if p[2][1] != None:
        instr.append(p[2][1])
    p[0] = [p[2][0], instr]

def p_program_empty(p):
    'program :'
    p[0] = [{}, []]
    pass

def p_line(p):
    'line : some_labels instr end'
    p[0] = [p[1], p[2]]


def p_some_labels_label(p):
    'some_labels : some_labels label'
    # Check if it is the first declaration of this label
    if p[2] in LABELS:
        print("Multiple declaration of label ", p[2])
        return False
    LABELS[p[2]] = CUR_ADDR
    
    # Check if this label was already used
    for k in UNRESOLVED.keys():
        if k == p[2]:
            # For each old use
            for l in UNRESOLVED[p[2]]:
                if INSTR[l][0] == 'data':
                    INSTR[l] = ('data', INSTR[l][1].replace(VAL_UNRESOLVED, str(CUR_ADDR)))
                else:
                    # For each operand
                    op = []
                    for o in INSTR[l][3]:
                        # New list of operand (tuples can't be modified)
                        ol = []
                        for e in o:
                            if isinstance(e, str):
                                e = e.replace(VAL_UNRESOLVED, str(CUR_ADDR))
                            ol.append(e)
                        # Transform list into tuple
                        op.append(tuple(ol))
                    INSTR[l] = (INSTR[l][0], INSTR[l][1].replace(VAL_UNRESOLVED, str(CUR_ADDR)), INSTR[l][2], op)

    p[0] = LABELS
    pass

def p_some_labels_empty(p):
    'some_labels :'
    p[0] = LABELS
    pass

def p_label(p):
    'label : IDENT COLON'
    p[0] = p[1]
    print("new label: " + p[1])
    pass

def p_instr(p):
    '''instr : instr_op0
             | instr_op1
             | instr_op2
             | '''

    # If instr wasn't empty
    if len(p) > 1:
        global CUR_ADDR
        # Store instructions
        INSTR[CUR_ADDR] = p[1]
        p[0] = p[1]

        # Increase current address
        CUR_ADDR += 1


def p_instr_op0(p):
    '''instr_op0 : NOP
                 | RESET
                 | RTI
                 | RTN
                 | TRAP'''
    p[0] = ("instr", str(p[1]), p[1], [])
    pass

def p_instr_op1_all(p):
    'instr_op1 : op1_all addmod_all'
    str_instr = str(p[1]) + " " + str(p[2][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1]])
    pass

def p_instr_op1_reg(p):
    'instr_op1 : op1_reg reg'
    str_instr = str(p[1]) + " " + str(p[2][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1]])
    pass

def p_instr_op1_push(p):
    '''instr_op1 : op1_push imm
                 | op1_push reg'''
    str_instr = str(p[1]) + " " + str(p[2][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1]])
    pass

def p_op1_all(p):
    '''op1_all : CALL
               | JMP
               | JEQ
               | JNE
               | JLE
               | JLT
               | JGE
               | JGT'''
    p[0] = p[1]
    pass

def p_op1_reg(p):
    '''op1_reg : NEG
               | NOT
               | POP'''
    p[0] = p[1]
    pass

def p_op1_push(p):
    'op1_push : PUSH'
    p[0] = p[1]
    pass

def p_instr_op2_all(p):
    'instr_op2 : op2_all addmod_all COMMA reg'
    str_instr = str(p[1]) + " " + str(p[2][0]) + str(p[3]) + str(p[4][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1], p[4][1]])
    pass

def p_instr_op2_restrict(p):
    'instr_op2 : op2_restrict addmod_dir_ind_idx COMMA reg'
    str_instr = str(p[1]) + " " + str(p[2][0]) + str(p[3]) + str(p[4][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1], p[4][1]])
    pass

def p_instr_op2_swap(p):
    '''instr_op2 : op2_swap reg COMMA reg
                 | op2_swap addmod_dir_ind_idx COMMA reg'''
    str_instr = str(p[1]) + " " + str(p[2][0]) + str(p[3]) + str(p[4][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1], p[4][1]])
    pass

def p_instr_op2_out(p):
    '''instr_op2 : op2_out imm COMMA addmod_dir_ind_idx
                 | op2_out reg COMMA addmod_dir_ind_idx'''
    str_instr = str(p[1]) + " " + str(p[2][0]) + str(p[3]) + str(p[4][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1], p[4][1]])
    pass

def p_instr_op2_st(p):
    'instr_op2 : op2_st reg COMMA addmod_dir_ind_idx'
    str_instr = str(p[1]) + " " + str(p[2][0]) + str(p[3]) + str(p[4][0])
    p[0] = ("instr", str_instr, p[1], [p[2][1], p[4][1]])
    pass

def p_op2_all(p):
    '''op2_all : ADD
               | AND
               | CMP
               | DIV
               | LD
               | MUL
               | OR
               | SHL
               | SHR
               | SUB
               | XOR'''
    p[0] = p[1]
    pass

def p_op2_restrict(p):
    '''op2_restrict : FAS
                    | IN'''
    p[0] = p[1]
    pass

def p_op2_swap(p):
    'op2_swap : SWAP'
    p[0] = p[1]
    pass

def p_op2_out(p):
    'op2_out : OUT'
    p[0] = p[1]
    pass

def p_op2_st(p):
    'op2_st : ST'
    p[0] = p[1]
    pass

def p_end(p):
    '''end : NEWLINE
           | SEMICOLON'''
    print('REDUCTION FIN D INSTR')
    p[0] = p[1]
    pass


# Each addmod return a tuple (string, parsed)
# The first part contain the string as it was given to the parser
# The second part is the result of the parsing as described at the beginning of this file
def p_addmod_imm(p):
    'imm : expr'
    s = p[1]
    p[0] = (s, ('imm', p[1]))
    pass

def p_addmod_reg(p):
    'reg : REG'
    s = "%" + p[1]
    p[0] = (s, ('reg', p[1]))
    pass

def p_addmod_dir(p):
    'dir : LBRACKET expr RBRACKET'
    s = p[1] + str(p[2]) + p[3]
    p[0] = (s, ('dir', p[2]))
    pass

def p_addmod_ind(p):
    'ind : LBRACKET REG RBRACKET'
    s = p[1] + "%" + p[2] + p[3]
    p[0] = (s, ('ind', p[2]))
    pass

def p_addmod_idx(p):
    '''idx : LBRACKET REG PLUS expr RBRACKET
           | LBRACKET REG MINUS expr RBRACKET'''
    n = int(p[3] + str(p[4]))
    s = p[1] + "%" + p[2] + p[3] + str(p[4]) + p[5]
    p[0] = (s, ('idx', p[2], n))
    pass

def p_addmod_all(p):
    '''addmod_all : imm
                  | reg
                  | dir
                  | ind
                  | idx'''
    p[0] = p[1]
    pass

def p_addmod_dir_ind_idx(p):
    '''addmod_dir_ind_idx : dir
                          | ind
                          | idx'''
    p[0] = p[1]
    pass

def p_expr(p):
    '''expr : NUMBER
            | expr_ident'''
    p[0] = p[1]
    pass

def p_expr_ident(p):
    'expr_ident : IDENT'
    if p[1] in LABELS:
        p[0] = LABELS[p[1]]
    else:
        if p[1] in UNRESOLVED:
            UNRESOLVED[p[1]].append(CUR_ADDR)
        else:
            UNRESOLVED[p[1]] = [CUR_ADDR]
        p[0] = VAL_UNRESOLVED
    pass


def p_error(p):
    print("Syntax error in input '{}'".format(p))

parser_file = yacc.yacc()
parser_instr = yacc.yacc(start='instr')

def parse(parser, instr, mem, labels={}, start=0, debug=False):
    global LABELS
    global INSTR
    global CUR_ADDR
    INSTR = mem
    LABELS = labels
    CUR_ADDR = start
    result = parser.parse(instr, debug=debug, tracking=True)
    print("UNRESOLVED: ", UNRESOLVED)
    return {'instr':INSTR, 'labels':LABELS}