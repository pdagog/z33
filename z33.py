from math import log10

# Size of the z33 memory
MEMSIZE              = 10000

# Memory location in which the context of the proc is saved
CONTEXT_SAVE_ADDR    = 100
# Memory location where to load the interruption subroutine for the proc
INTERRUPT_SUBROUTINE = 200

# Return value resulting from the execution of an instruction
NO_PC_INC                    = -1 # Returned when PC don't need to be increment
INTERRUPT_HARDWARE           = 1  # Returned after an hardware interruption
INTERRUPT_DIVIDE8_BY_ZERO    = 2  # Returned after an attemp to divide by zero
INTERRUPT_INVALID_INSTR      = 3  # Returned after an invlide instruction
INTERRUPT_SUPERUSER_REQUIRED = 4  # Returned after attempting to execute an instruction needing superuser privileges without them
INTERRUPT_TRAP               = 5  # Returned after a trap
INTERRUPT_INVALID_MEM_ACCESS = 6  # Returned after an invalid access to memory

# Starting code segment
INIT_ADDR                    = 100

# Position of bits in the Status Register
STATUS = {
  "C":  0,
  "Z":  1,
  "N":  2,
  "O":  3,
  "IE": 8,
  "S":  9,
}

# List of all recognize instructions
recognized = [
    'add',
    'and',
    'call',
    # 'cmp',
    'div',
    'fas',
    # 'in',
    # 'jeq',
    # 'jge',
    # 'jgt',
    # 'jle',
    # 'jlt',
    'jmp',
    # 'jne',
    'ld',
    'mul',
    # 'neg',
    'nop',
    'not',
    'or',
    # 'out',
    'pop',
    'push',
    'reset',
    # 'rti',
    'rtn',
    'shl',
    'shr',
    'st',
    'sub',
    'swap',
    # 'trap',
    'xor',
]


# Return a value whith the same format as if it was given by the parser
def create_data(value):
  return ('data', value)

# The virtual z33
class Z33():
  def __init__(self, proc_tuple=(0,0,0,0,0), prog={'instr':[],'labels':{}}, update_gui=(lambda *args: None)):
    self.proc = Processor(*proc_tuple)
    self.mem = Memory(prog)
    self.ready = True
    self.update_gui = update_gui
    self.nb_instr_exec = 0

  # Enable deppcopy
  def __reduce__(self):
    proc_tuple = (self.proc.a[1], self.proc.b[1], self.proc.pc[1], self.proc.sp[1], self.proc.sr[1])
    prog = {'instr':self.mem.cells, 'labels':self.mem.labels}
    return (self.__class__, (proc_tuple, prog, None))

  # Make this class printable (for debug)
  def __repr__(self):
    return "Z33()"

  def __str__(self):
    r =  self.proc.__str__()
    r += "\n"
    r += self.mem.__str__()
    r += "\n"
    return r


# Return the content of an argument parsing its addressing mode
  def parse_addr_mode(self, arg):
    # Immediate addressing mode
    if arg[0] == "imm":
      return ('imm', arg[1])
    
    # Register addressing mode
    elif arg[0] == "reg":
      # Uknown register
      if arg[1] not in ['a', 'b', 'pc', 'sp', 'sr']:
        return None
      
      return ('reg', arg[1])
    
    # Direct addressing mode
    elif arg[0] == "dir":
      return ('mem', arg[1])

    # Indirect addressing mode
    elif arg[0] == "ind":
      reg = self.proc.get_accessor(arg[1])
      
      # Register should contain data
      if reg[0] != 'data':
        print("Register should contain data for indirect memory access")
        return None
      
      return ('mem', reg[1])

    # Indirect indexed addressing mode
    elif arg[0] == "idx":
      reg = self.proc.get_accessor(arg[1])
      
      # Register should contain data
      if reg[0] != 'data':
        print("Register should contain data for indirect indexed memory access")
        return None

      return ('mem', reg[1] + int(arg[2]))

    # Unknwown addressing mode
    else:
      print("Unknwown adressing mode")
      return None


  # Read the value from register or memory
  def read_value(self, arg):
    # Check arg
    if type(arg) != tuple:
      return None
    
    if arg[0] == 'imm':
      return create_data(arg[1])
    
    if arg[0] == 'reg':
      return self.proc.get_accessor(arg[1])
    
    if arg[0] == 'mem':
      return self.mem.cells[arg[1]]
    
    return None


  # Return the value from register or memory
  def write_value(self, loc, val):
    # Check arg
    if type(loc) != tuple:
      return None
    
    if loc[0] == 'imm':
      print("Could not return a location from an immediate addressing mode")
      return None
    
    if loc[0] == 'reg':
      return self.proc.get_mutator(loc[1])(val)
    
    if loc[0] == 'mem':
      return self.mem.store(idx=loc[1], data=val)
    
    return None



  # Execute the instruction given as parameter
  def execute(self, instr):
    # Check if the instruction is recognized
    if instr[0] != "instr" or instr[2] not in recognized:
      print("Not an instr")
      err = INTERRUPT_INVALID_INSTR
    
    else:
      print("Executing " + instr[1])
      err = getattr(self, "z_" + instr[2])(instr[3])

    # Check if an error occured
    if err > 0:
      # Start interrupt subroutine
      self.interrupt_exception(err)
    elif err == 0:
      # Increment program counter
      self.proc.pc = create_data(self.proc.pc[1] + 1)
    # else: Some cases dont need pc to be increment (ex jmp, call...)


    # Increment the number of instruction executed
    self.nb_instr_exec += 1

    # Update the GUI
    self.update_gui()


  def interrupt_exception(self, code):
    print("Starting of interrupt subroutine")

    # Store register PC at address 100
    self.mem.cells[CONTEXT_SAVE_ADDR] = self.proc.pc

    # Store register SR at address 101
    self.mem.cells[CONTEXT_SAVE_ADDR+1] = self.proc.sr

    # Store event code at address 102
    self.mem.cells[CONTEXT_SAVE_ADDR+2] = create_data(code)

    # set SR:S
    self.proc.set_status(STATUS['S'], 1)

    # unset SR:IE if it is an interrupt
    if code == 0:
      self.proc.set_status(STATUS['IE'], 0)
    
    # Set PC to 200
    self.proc.pc = create_data(INTERRUPT_SUBROUTINE)

  
  def z_add(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op1[1] + op2[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_and(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op1[1] & op2[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_call(self, args):
    val = self.read_value(self.parse_addr_mode(args[0]))

    # Check destination address
    if val == None or val[0] != 'data':
      return INTERRUPT_INVALID_INSTR

    # Push pc
    self.proc.sp = create_data(self.proc.sp[1] - 1)
    self.mem.store(self.proc.sp[1], self.proc.pc)

    # Jump
    self.proc.pc = val
    
    return NO_PC_INC
  
  def z_cmp(self, args):
    return 0
  
  def z_div(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op2[1] / op1[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_fas(self, args):
    reg = self.parse_addr_mode(args[1])
    mem = self.parse_addr_mode(args[0])
    val = self.read_value(mem)
    if val == None:
      return INTERRUPT_INVALID_INSTR

    # fetch memory in register
    err = self.write_value(reg, val)
    if err == None:
      return INTERRUPT_INVALID_INSTR

    # and set memory to 1
    data = create_data(1)
    err = self.write_value(mem, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0

  def z_in(self, args):
    return 0
  
  def z_jeq(self, args):
    return 0
  
  def z_jge(self, args):
    return 0
  
  def z_jgt(self, args):
    return 0
  
  def z_jle(self, args):
    return 0
  
  def z_jlt(self, args):
    return 0
  
  def z_jmp(self, args):
    val = self.read_value(self.parse_addr_mode(args[0]))

    # Check destination address
    if val == None or val[0] != 'data':
      return INTERRUPT_INVALID_INSTR

    # Jump
    self.proc.pc = val

    return NO_PC_INC
  
  def z_jne(self, args):
    return 0
  
  def z_ld(self, args):
    # Parse addressing mode
    loc = self.parse_addr_mode(args[0])

    # Get value from memory
    val = self.read_value(loc)
    if val is None:
      return INTERRUPT_INVALID_INSTR

    # Store value in register
    dest = self.parse_addr_mode(args[1])
    err = self.write_value(dest, val)
    if err is None:
      return INTERRUPT_INVALID_INSTR

    return 0

  def z_mul(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op1[1] * op2[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_neg(self, args):
    return 0
  
  def z_nop(self, args):
    return 0
  
  def z_not(self, args):
    reg = self.parse_addr_mode(args[0])
    val = self.read_value(reg)

    if val == None or val[0] != 'data':
      return INTERRUPT_INVALID_INSTR

    data = create_data(~val[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_or(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op1[1] | op2[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_out(self, args):
    return 0
  
  def z_pop(self, args):
    reg = self.parse_addr_mode(args[0])

    # Check destination address
    if reg == None:
      return INTERRUPT_INVALID_INSTR

    # Pop
    self.write_value(reg, self.mem.cells[self.proc.sp[1]])
    self.proc.sp = create_data(self.proc.sp[1] + 1)

    return 0
  
  def z_push(self, args):
    val = self.read_value(self.parse_addr_mode(args[0]))

    # Check destination address
    if val == None:
      return INTERRUPT_INVALID_INSTR

    # Push
    self.proc.sp = create_data(self.proc.sp[1] - 1)
    self.mem.store(self.proc.sp[1], val)

    return 0
  
  def z_reset(self, args):
    data = create_data(0)
    self.proc.a = data
    self.proc.b = data
    self.proc.pc = data
    self.proc.sp = data
    self.proc.sr = data
    return 0
  
  def z_rti(self, args):
    return 0
  
  def z_rtn(self, args):
    # Pop
    self.proc.pc = self.mem.cells[self.proc.sp[1]]
    self.proc.sp = create_data(self.proc.sp[1] + 1)
    
    return 0
  
  def z_shl(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op2[1] << op1[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_shr(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op2[1] >> op1[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_st(self, args):
    # Parse addressing mode
    loc = self.parse_addr_mode(args[0])
    
    # Get value from register
    val = self.read_value(loc)
    if val == None:
      return INTERRUPT_INVALID_INSTR
    
    # Store value in memory
    dest = self.parse_addr_mode(args[1])
    err = self.write_value(dest, val)
    if err == None:
      return INTERRUPT_INVALID_INSTR

    return 0
  
  def z_sub(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op2[1] - op1[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_swap(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    val1 = self.read_value(src)
    val2 = self.read_value(reg)

    if val1 == None or val2 == None:
      return INTERRUPT_INVALID_INSTR

    err = self.write_value(reg, val1)
    if err == None:
      return INTERRUPT_INVALID_INSTR

    err = self.write_value(src, val2)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  
  def z_trap(self, args):
    return 0
  
  def z_xor(self, args):
    reg = self.parse_addr_mode(args[1])
    src = self.parse_addr_mode(args[0])
    op1 = self.read_value(src)
    op2 = self.read_value(reg)

    if op1 == None or op2 == None:
      return INTERRUPT_INVALID_INSTR

    # Check that we are computing datas
    if op1[0] != 'data' or op2[0] != 'data':
      return INTERRUPT_INVALID_INSTR
    
    data = create_data(op1[1] ^ op2[1])
    err = self.write_value(reg, data)
    if err == None:
      return INTERRUPT_INVALID_INSTR
    
    return 0
  


# A virtual processor
class Processor():
  def __init__(self, a=0, b=0, pc=0, sp=0, sr=0):
    # Registers
    self._a = create_data(a)
    self._b = create_data(b)
    self._pc = create_data(pc)
    self._sp = create_data(sp)
    self._sr = create_data(sr)

  
  # Make this class printable (for debug)
  def __repr__(self):
    return "Processor()"

  def __str__(self):
    r = "reg: "
    r += "\n   a: " + str(self.a)
    r += "\n   b: " + str(self.b)
    r += "\n  pc: " + str(self.pc)
    r += "\n  sp: " + str(self.sp)
    r += "\n  sr: " + str(self.sr)

    return r


  # Return the setter on a given register 
  def get_mutator(self, reg):
    if reg == "a":
      return self.set_a
    elif reg == "b":
      return self.set_b
    elif reg == "pc":
      return self.set_pc
    elif reg == "sp":
      return self.set_sp
    elif reg == "sr":
      return self.set_sr
    else:
      return None


  # Return the accessor on a given register
  def get_accessor(self, reg):
    if reg == "a":
      return self.a
    elif reg == "b":
      return self.b
    elif reg == "pc":
      return self.pc
    elif reg == "sp":
      return self.sp
    elif reg == "sr":
      return self.sr
    else:
      return None

  @property
  def a(self):
    return self._a

  @a.setter
  def a(self, value):
    self._a = value

  def set_a(self, value):
    self.a = value
    return 0

  @property
  def b(self):
    return self._b

  @b.setter
  def b(self, value):
    self._b = value

  def set_b(self, value):
    self.b = value
    return 0

  @property
  def pc(self):
    return self._pc

  @pc.setter
  def pc(self, value):
    self._pc = value

  def set_pc(self, value):
    self.pc = value
    return 0

  @property
  def sp(self):
    return self._sp

  @sp.setter
  def sp(self, value):
    self._sp = value

  def set_sp(self, value):
    self.sp = value
    return 0

  @property
  def sr(self):
    return self._sr

  @sr.setter
  def sr(self, value):
    self._sr = value

  def set_sr(self, value):
    self.sr = value
    return 0

  def set_status(self, status, value):
    mask = 1 << status
    self.sr = create_data((self.sr[1] & ~mask) | ((value << status) & mask))

  def get_status(self, status):
    mask = 1 << status
    return (self.sr[1] & mask) != 0 



# Virtual memory
class Memory():
  def __init__(self, prog={'instr':[], 'labels':{}}):
    if len(prog['instr']) > MEMSIZE:
      # Truncate if overflow
      self.cells = prog['instr'][:MEMSIZE]
    else:
      # Fill empty memory with zeros
      self.cells = prog['instr'] + [create_data(0)]*(MEMSIZE - len(prog['instr']))

    self.labels = prog['labels']

  # Make this class printable (for debug)
  def __repr__(self):
    return "Memory()"

  def __str__(self):
    lab = {}
    if len(self.labels) > 0:
      # Create coma separated labels
      for i in self.labels:
        if self.labels[i] in lab:
          lab[self.labels[i]] += ", " + i
        else:
          lab[self.labels[i]] = i
      
      maxspaces = max(len(l) for p, l in lab.items())

    else:
      maxspaces = 0

    r = "mem: "
    for i in range(MEMSIZE):
      if i in self.labels.values():
        name = ""
        for key in self.labels:
          if self.labels[key] == i:
            name += key + ", "
        name = name[:-2]
        r+= '\n  ' + ' '*(maxspaces-len(name)) + '"' + name + " "
      else:
        r += "\n  " + ' ' * (maxspaces+2)
      
      pad = int(log10(MEMSIZE)) if (i == 0) else (int(log10(MEMSIZE))-int(log10(i)))
      r += "[" + str(i) + "]: " + (" "*pad)
      r += str(self.cells[i])

    return r

  def store(self, idx, data):
    self.cells[idx] = data
    return 0
