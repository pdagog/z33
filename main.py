#!/usr/bin/env python
import sys
from gui import Zemulator
from z33 import Z33, MEMSIZE, INIT_ADDR
from parser import parser_file, parse
from subprocess import Popen, PIPE


def main():
  # Create the virtual zorglub
  emu = Z33((0,0,0,0,0))
  
  # Check given args
  if(len(sys.argv) < 2):
    file = "None"
    prog = {'instr':[], 'labels':{}}
  else:
    # Parse the sourcecode file
    file = sys.argv[1]
    prog = init_from_file(file, emu.mem)

  # Create the GUI
  emulator = Zemulator(emu=emu, file=file)

  # Link gui and emulator
  emu.update_gui = emulator.update

  # Start the gui
  emulator.run()
  print(emu)


# Init emu's memory from a file
def init_from_file(file, mem):
  prog = {}
  prog['instr'] = []
  prog['labels'] = {}
  
  # Call g++
  proc = Popen(['cpp', file], stdout=PIPE, universal_newlines=True)

  # Pass g++'s output to parser
  (out, err) = proc.communicate()
  parsed = parse(parser=parser_file, instr=out, labels=mem.labels,
                 mem=mem.cells, start=INIT_ADDR)
  mem.labels = parsed['labels']


if __name__ == "__main__":
  main()