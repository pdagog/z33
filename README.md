# z33

Zorglub33 emulator

## Reference material

  * [OS course](https://pdagog.gitlab.io/ens/cours-ase.pdf)
  * [Quick reference card](https://pdagog.gitlab.io/ens/z33refcard.pdf)

## License

CeCill-B. See LICENSE file.
