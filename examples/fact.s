; calcul de n! (n passé en argument)
factorielle:
  ld   (%sp+1),%a
  cmp  1,%a
  jge  casparticulier  ; saut si 1≥n
  
  sub  1,%a
  push %a
  call factorielle
  add  1,%sp
  push %b
  ld   (%sp+1),%b
  mul  %b,%a
  pop  %b
  rtn
  
casparticulier:
  ld   1,%a
  rtn
