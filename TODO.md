# TODO

Not all tasks take the same time...


## General 

- [ ] Write a user guide
- [ ] Clean the code (this one is a HUGE job)
- [ ] Use a correct pipe between *cpp* and the parser (is there a PLY arg ?)

## Emulator
  - [ ] Check for privileges when executing instructions
  - [ ] Implement *cmp*, *in*, *jxx*, *neg*, *out*, *rti*, *trap*
  - [ ] Implement other *fas* versions
  - [ ] Update SR register when an instruction is executed (in write_value ?)
  - [ ] Add I/O features
  - [ ] Add run, stop, continue features
  - [ ] Add breakpoints management
  - [ ] Zorglub33-M
  - [ ] Zorglub33-S
  - [ ] Zorglub33-P

## GUI
  - [ ] URGENT: Fix IHM bug
  - [ ] Color in red the boxes containing an instruction not recognized
        by the analyzer

## Parser
  - [ ] Only create 1 (common) LALR table for the 2 parser
  - [ ] Add a rule in Yacc to distinguish good and wrong registers
  - [ ] Error managment
    - [ ] Track current file in lexer
    - [ ] manage error in parser (cf PLY's documentation, paragraph 6.8.1)
  - [ ] Recognize assembler directives
  - [ ] Get labels working in idx addressing mode (ex ld [%a+LABEL],%b)

