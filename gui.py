from kivy.app import App
from kivy.uix.image import Image
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.vkeyboard import VKeyboard
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton, ToggleButtonBehavior
from kivy.uix.textinput import TextInput
from kivy.graphics import Color, Rectangle

from kivy.properties import StringProperty, NumericProperty
import traceback

import re                                   # Regular Expressions
from copy import deepcopy
from color import *
from z33 import Z33, Memory, STATUS
from parser import parser_instr, parse


# Maximum number of characters of a label list displayed
LABEL_MAX_SIZE = 12


KV = '''
<MemoryCell>
    canvas.before:
        Color:
            rgba: (0.5,0.5,0.5,1)
        Line:
            width: 1
            rectangle: self.x, self.y, self.width, self.height


<MemoryTable>:
    data: []
    viewclass: 'MemoryCell'
    RecycleBoxLayout:
        default_size: None, dp(30)
        default_size_hint: 1, None
        size_hint_y: None
        height: self.minimum_height
        orientation: 'vertical'
        spacing: 3

<IntInput>:
    dec: dec
    hex: hex

    DecInput:
        id: dec
        on_text: root.changeval(args[1])

    HexInput:
        id: hex
        on_text: root.changeval(args[1])
'''



# The application's main window
class MainWindow(GridLayout):
  def __init__(self, emu=Z33(), file="", **kwargs):
    super(MainWindow, self).__init__(**kwargs)
    self.rows = 3
    self.spacing = 10
    self.padding = 5

    # Add different frames
    self.titlebar = TitleBar(size_hint_y=None, height=30, file=file)
    self.registers = RegistersPanel(proc=emu.proc)
    self.memory = MemoryView(title="Memory", emu=emu)
    self.instructions = MemoryView(title="Instructions", emu=emu, follow=True)
    self.output = Output(size_hint=(0.7, 0.5), pos_hint={'center_x':.5, 'center_y':.5})
    self.tools = DebuggerTools(emu=emu)
    self.input = Keyboard()

    # Define position of all widgets 
    regandmem = BoxLayout(orientation="vertical", spacing=10, size_hint_x=0.6)
    regandmem.add_widget(self.registers)
    regandmem.add_widget(self.instructions)

    center = BoxLayout(orientation="horizontal", spacing=10, size_hint_y=0.7)
    center.add_widget(regandmem)
    center.add_widget(self.memory)
    center.add_widget(self.output)

    bottom = BoxLayout(orientation="horizontal", spacing=10, size_hint_y=0.3)
    bottom.add_widget(self.tools)
    bottom.add_widget(self.input)

    self.add_widget(self.titlebar)
    self.add_widget(center)
    self.add_widget(bottom)

    # Original z33 and memory persistent view
    self.emu = emu
    self.mem_view = deepcopy(emu)

    # Background color feature
    with self.canvas.before:
      Color(*BACKGROUND)  # green; colors range from 0-1 not 0-255
      self.rect = Rectangle(size=self.size, pos=self.pos)

    # Update the background color on resize
    self.bind(size=self._update_bg, pos=self._update_bg)

  # Update the background color
  def _update_bg(self, instance, value):
    self.rect.pos = instance.pos
    self.rect.size = instance.size

  # Update the gui
  def update(self):
    self.titlebar.update(self.emu.nb_instr_exec)
    self.update_registers()
    self.update_memory()

    # Update persistent memory view
    self.mem_view = deepcopy(self.emu)

  # Update the view of registes
  def update_registers(self):
    self.registers.update(self.mem_view)
  
  # Update the view of memory
  def update_memory(self):
    self.memory.update()
    # self.instructions.update()



class ImageToggleButton(ToggleButtonBehavior, Image):
  def __init__(self, img_up, img_down, **kwargs):
    super(ImageToggleButton, self).__init__(**kwargs)
    self.img_up = img_up
    self.img_down = img_down
    self.source = img_up

  def on_state(self, widget, value):
    if value == 'down':
      self.source = self.img_down
    else:
      self.source = self.img_up



# The top widget containing the program's title
class TitleBar(BoxLayout):
  def __init__(self, file="", **kwargs):
    super(TitleBar, self).__init__(**kwargs)
    self.cols = 3

    self.filename = Label(halign="left", valign="middle")
    self.filename.text = "Filename: " + file

    self.time = 0
    self.cd = 0
    self.time_label = Label(halign="right", valign='middle', size_hint_x=None)
    self.time_label.size_x = self.time_label.width
    self.time_label.text = "Time: " + str(self.time - self.cd)

    self.cd_button = ImageToggleButton(img_up="img/countdown.webp", img_down="img/countdown_down.png", size_hint_x=None, width=self.height)
    self.cd_button.bind(state=self.update_cd)

    self.add_widget(self.filename)
    self.add_widget(self.cd_button)
    self.add_widget(self.time_label)

    # Background color feature
    with self.canvas.before:
      Color(*TITLEBAR)  # green; colors range from 0-1 not 0-255
      self.rect = Rectangle(size=self.size, pos=self.pos)

    # Update the background color on resize
    self.bind(size=self._update_bg, pos=self._update_bg)

  # Update the background color
  def _update_bg(self, instance, value):
    self.rect.pos = instance.pos
    self.rect.size = instance.size

  # Update current number of executed instruction
  def update(self, value):
    self.time = value
    self.time_label.text = "Time: " + str(self.time - self.cd)

  def update_cd(self, instance, value):
    if value == 'down':
      self.cd = self.time
    else:
      self.cd = 0
    
    self.update(self.time)



# A widget showing the value of a register
class Register(GridLayout):
  def __init__(self, proc=None, ref="", **kwargs):
    super(Register, self).__init__(**kwargs)
    self.cols = 3

    # Fix the register's row height
    self.size_hint_y = None
    self.height = 30

    self.proc = proc
    self.reg = ref.lower()
    self.label = Label(text=ref, size_hint_x=None, width=30)
    self.inputs = IntInput(text='0')
    self.inputs.dec.bind(text=self.update_proc)

    self.add_widget(self.label)
    self.add_widget(self.inputs)

  # Update the register shown value to the z33's actual value
  def update(self, val):
    self.inputs.changeval(str(val))

  # Update the processor values to correspond to the users entry
  def update_proc(self, instance, value):
    # Manage empty case
    if value == "":
      value = '0'
    
    # reg[0]:data --> reg[1]:int, reg[0]:instr --> reg[1]:string
    if isInt(value):
      self.proc.get_mutator(self.reg)(("data", int(value)))
    else:
      instr = self.proc.get_accessor(self.reg)
      lst = list(instr)
      lst[1] = value
      instr = tuple(lst)
      self.proc.get_mutator(self.reg)(instr)



# The input for each bit of the status register
class BitInput(TextInput):
  pat = re.compile('[^0-1]')
  val = NumericProperty()
  
  def __init__(self, proc=None, status=0, **kwargs):
    super(BitInput, self).__init__(**kwargs)
    self.root = App.get_running_app()
    self.proc = proc
    self.status = status
    self.val = int(proc.get_status(status))
    self.multiline = False
    self.input_filter = "int"
    self.text = str(self.val)
  
  # Limit input to only 1 bit
  def insert_text(self, substring, from_undo=False):
    if self.text == "":
      pat = self.pat
      s = re.sub(pat, '1', substring)
      return super(BitInput, self).insert_text(s, from_undo=from_undo)

  # Change SR on text change
  def on_text(self, instance, value):
    if value == "":
      self.val = 0
    else:
      self.val = int(value)

  # Automagically called when val is modified
  def on_val(self, instance, value):
    self.proc.set_status(self.status, value)
    self.root.mw.registers.sr.update(self.proc.sr[1])
  

# One status input and its label
class StatusBit(GridLayout):
  def __init__(self, proc=None, ref="", status=0, **kwargs):
    super(StatusBit, self).__init__(**kwargs)
    self.cols = 2

    self.idx = status
    self.label = Label(text=ref, size_hint_x=None, width=30)

    self.input = BitInput(proc=proc, status=status)

    self.add_widget(self.label)
    self.add_widget(self.input)

  # Update to z33's current value
  def update(self, value):
    self.input.text = str(value)


# Container for all status inputs
class StatusRegister(GridLayout):
  def __init__(self, proc=None, **kwargs):
    super(StatusRegister, self).__init__(**kwargs)
    self.cols = 6

    # Fix the register's row height
    self.size_hint_y = None
    self.height = 30

    self.val = proc.sr
    
    self.s = StatusBit(proc=proc, ref="S", status=STATUS['S'])
    self.ie = StatusBit(proc=proc, ref="IE", status=STATUS['IE'])
    self.o = StatusBit(proc=proc, ref="O", status=STATUS['O'])
    self.n = StatusBit(proc=proc, ref="N", status=STATUS['N'])
    self.z = StatusBit(proc=proc, ref="Z", status=STATUS['Z'])
    self.c = StatusBit(proc=proc, ref="C", status=STATUS['C'])

    self.add_widget(self.s)
    self.add_widget(self.ie)
    self.add_widget(self.o)
    self.add_widget(self.n)
    self.add_widget(self.z)
    self.add_widget(self.c)


  # Return a particular bit located in bit given by status parameter
  def get_status(self, status):
    mask = 1 << status
    return int((self.val & mask) != 0)

  # Update the sr register to correspond to the z33 actual value
  def update(self, value):
    if self.val != value:
      self.val = value
      self.s.update(self.get_status(STATUS['S']))
      self.ie.update(self.get_status(STATUS['IE']))
      self.o.update(self.get_status(STATUS['O']))
      self.n.update(self.get_status(STATUS['N']))
      self.z.update(self.get_status(STATUS['Z']))
      self.c.update(self.get_status(STATUS['C']))


class StatusRegisterLayout(GridLayout):
  val = NumericProperty()
  
  def __init__(self, proc=None, ref="SR", **kwargs):
    super(StatusRegisterLayout, self).__init__(**kwargs)
    self.cols = 2

    self.proc = proc
    self.label = Label(text=ref, size_hint_x=None, width=30)
    self.padding = (0, 10, 0, 0)
    
    self.inputs = GridLayout(rows=2)
    self.status = StatusRegister(proc=proc)
    self.hex = HexInput()
    self.inputs.add_widget(self.status)
    self.inputs.add_widget(self.hex)

    self.add_widget(self.label)
    self.add_widget(self.inputs)

    self.hex.bind(text=self.on_hex)
    self.val = int(proc.sr[1])
    self.hex.text = "0x{:03x}".format(self.val)


  # Change SR on text change
  def on_hex(self, instance, value):
    if value == "" or (not isInt(value, base=16)):
      self.hex.text = "0x{:03x}".format(0)
    else:
      self.val = int(value, base=16)

  # Automagically called when val is modified
  def on_val(self, instance, value):
    self.hex.text = "0x{:03x}".format(value)
    self.proc.sr = ('data', value)
    self.status.update(value)

  def update(self, value):
    self.val = value



# The widget showing registers current value
class RegistersPanel(BoxLayout):
  def __init__(self, proc=None, **kwargs):
    super(RegistersPanel, self).__init__(**kwargs)
    self.padding = 5
    self.spacing = 3
    self.orientation = "vertical"
    self.size_hint_y = None
    self.height = 212
    self.proc = proc
    
    self.a = Register(proc=proc, ref="A")
    self.b = Register(proc=proc, ref="B")
    self.pc = Register(proc=proc, ref="PC")
    self.sp = Register(proc=proc, ref="SP")
    self.sr = StatusRegisterLayout(proc=proc)

    self.add_widget(self.a)
    self.add_widget(self.b)
    self.add_widget(self.pc)
    self.add_widget(self.sp)
    self.add_widget(self.sr)

    # Background color feature
    with self.canvas.before:
      Color(*REGISTERS)  # green; colors range from 0-1 not 0-255
      self.rect = Rectangle(size=self.size, pos=self.pos)

    # Update the background color on resize
    self.bind(size=self._update_bg, pos=self._update_bg)

  # Update the background color
  def _update_bg(self, instance, value):
    self.rect.pos = instance.pos
    self.rect.size = instance.size

  # Update registers
  def update(self, view):
    if view.proc.a != self.proc.a:
      self.a.update(self.proc.a[1])
    if view.proc.b != self.proc.b:
      self.b.update(self.proc.b[1])
    if view.proc.pc != self.proc.pc:
      self.pc.update(self.proc.pc[1])
    if view.proc.sp != self.proc.sp:
      self.sp.update(self.proc.sp[1])
    if view.proc.sr != self.proc.sr:
      self.sr.update(self.proc.sr[1])


# A view of the memory
class MemoryView(GridLayout):
  def __init__(self, emu=Z33(), follow=False, **kwargs):
    super(MemoryView, self).__init__()
    self.rows = 2
    self.spacing = 10
    self.padding = 5
    self.follow = follow
    self.root = App.get_running_app()
    self.emu = emu

    # Add table of memory cell
    self.table = MemoryTable(emu)
    self.add_widget(self.table)

    # Add research option
    search_layout = BoxLayout(orientation='horizontal', spacing=10, size=(30,30), size_hint=(1, None))
    search_layout.add_widget(Label(text="Go to word cell: "))
    self.search = DecInput()
    if follow:
      self.search.text = str(emu.proc.pc[1])
    self.search.bind(text=self.goto)
    search_layout.add_widget(self.search)
    self.add_widget(search_layout)

    # Background color feature
    with self.canvas.before:
      Color(*MEMORY)  # green; colors range from 0-1 not 0-255
      self.rect = Rectangle(size=self.size, pos=self.pos)

    # Update the background color on resize
    self.bind(size=self._update_bg, pos=self._update_bg)

  # Update the background color
  def _update_bg(self, instance, value):
    self.rect.pos = instance.pos
    self.rect.size = instance.size


  def goto(self, instance, value):
    if value == "":
      value = 0
    self.table.scroll_to_index(int(value))

  # Update the memory view
  def update(self):
    self.table.update()

    # Display instruction line if follow is set
    if self.follow == True:
      self.search.text = str(self.emu.proc.pc[1])

  # Update the z33
  def update_memory(self, index, data):
    self.emu.mem.store(index, data)
    self.root.mw.update_memory()
    return


# The table of memory word
class MemoryTable(RecycleView):
  def __init__(self, emu=Z33(), **kwargs):
    super(MemoryTable, self).__init__(**kwargs)

    self.emu = emu
    self.data = []
    for row in range(len(emu.mem.cells)):
      labels = ""
      # Add labels
      if row in emu.mem.labels:
        labels = emu.mem.labels[row] + ': '
        # Trunc labels if overflow
        if len(labels) > LABEL_MAX_SIZE:
          labels = labels[:9] + "... : "

      cell = {"idx":str(row), "value":emu.mem.cells[row], "labels":labels}
      self.data.append(cell)

  # Modify one memorycell
  def store(self, idx, val):
    self.data[idx]['value'] = val

  def scroll_to_index(self, index):
    box = self.children[0]
    pos_index = (box.default_size[1] + box.spacing) * index
    scroll = self.convert_distance_to_scroll(0, pos_index - (self.height * 0.5))[1]
    if scroll > 1.0:
      scroll = 1.0
    elif scroll < 0.0:
      scroll = 0.0
    self.scroll_y = 1.0 - scroll

  # Update printing of the z33 state
  def update(self):
    refresh = False
    
    for i in range(len(self.data)):
      if self.data[i]['value'] != self.emu.mem.cells[i]:
        # print("idx: ", i)
        # print("loc: ", self.data[i])
        # print("mem: ", self.emu.mem.cells)
        self.store(i, self.emu.mem.cells[i])
        # print("chg: ", self.data[i])
        # print()
        refresh = True
        
    if refresh:
      # Force recycle view to refresh its display
      self.refresh_from_data()


class MemoryCell(RecycleDataViewBehavior, BoxLayout):
  def __init__(self, **kwargs):
    super(MemoryCell, self).__init__(**kwargs)

    self.root = App.get_running_app()
    self.label = Label(padding=(5,0), valign='middle', halign='right', size_hint_x=None, width=100)
    self.label.text_size = self.label.size
    self.inputs = IntInput()
    self.inputs.dec.bind(text=self.update_mem)
    self.add_widget(self.label)
    self.add_widget(self.inputs)
  
    self.idx = 0

  # Called when the Memory cell is turn visible on screen
  def refresh_view_attrs(self, rv, idx, data):
    print("refresh: ", idx, " ", data, " ", self.idx, " ", data['idx'])
    self.idx = idx
    self.label.text = data['labels'] + data['idx']
    self.inputs.changeval(str(data['value'][1]))
    return super(MemoryCell, self).refresh_view_attrs(rv, idx, data)

  # Update the processor values to correspond to the users entry
  def update_mem(self, instance, value):
    # Manage creation case (id is still empty)
    if self.idx is None:
      return

    print("update cell: ", self.idx, ", ", value)
    
    labels = self.root.mw.emu.mem.labels

    # Manage empty case
    if value == "":
      data = ('data', 0)

    elif isInt(value):
      data = ('data', int(value))
    else:
      parsed = parse(parser_instr, instr=value, start=self.idx, mem=self.root.mw.emu.mem.cells, labels=labels)
      
      # Instruction incorrect
      if not parsed['instr']:
        return
      
      data = parsed['instr'][0]

    self.root.mw.memory.update_memory(int(self.idx), data)


class Output(Widget):
  def __init__(self, **kwargs):
    super(Output, self).__init__(**kwargs)
    with self.canvas:
      Color(0, 0, 1, mode='rgb')
      self.rect = Rectangle(pos=self.pos, size=self.size)
      self.bind(pos=self.update_screen)
      self.bind(size=self.update_screen)

  def update_screen(self, *args):
    self.rect.pos = self.pos
    self.rect.size = self.size


class DebuggerTools(BoxLayout):
  def __init__(self, emu=Z33(), **kwargs):
    super(DebuggerTools, self).__init__(**kwargs)
    self.orientation = 'horizontal'
    self.emu = emu

    vlayout = BoxLayout(orientation='vertical')
    self.run = Button(text="run")
    
    self.step = Button(text="step")
    self.step.bind(on_press=self.step_callback)

    vlayout.add_widget(self.run)
    vlayout.add_widget(self.step)

    self.br = BreakView()

    self.add_widget(vlayout)
    self.add_widget(self.br)

  # One assembly step
  def step_callback(self, instance):
    instr = self.emu.mem.cells[int(self.emu.proc.pc[1])]
    self.emu.execute(instr)


class BreakView(BoxLayout):
  def __init__(self, **kwargs):
    super(BreakView, self).__init__(**kwargs)
    self.orientation = 'vertical'
    self.list = RecycleView()

    hlayout = BoxLayout(orientation='horizontal', size=(30,30), size_hint=(1, None))
    self.search = DecInput()
    self.add = Button(text='break')
    self.clear = Button(text='clear')
    hlayout.add_widget(self.search)
    hlayout.add_widget(self.add)
    hlayout.add_widget(self.clear)

    self.add_widget(self.list)
    self.add_widget(hlayout)

    # Background color feature
    with self.canvas.before:
      Color(*BREAKLIST)  # green; colors range from 0-1 not 0-255
      self.rect = Rectangle(size=self.size, pos=self.pos)

    # Update the background color on resize
    self.bind(size=self._update_bg, pos=self._update_bg)

  # Update the background color
  def _update_bg(self, instance, value):
    self.rect.pos = instance.pos
    self.rect.size = instance.size


class Keyboard(VKeyboard):
  def __init__(self, **kwargs):
    super(Keyboard, self).__init__(**kwargs)


# Restrict input text to Int in decimal
class DecInput(TextInput):
  def __init__(self, **kwargs):
    super(DecInput, self).__init__(**kwargs)
    self.multiline = False

    # Force integer inputs
    self.input_filter = "int"


# Restrict input text to Int in hexadecimal
class HexInput(TextInput):
  def __init__(self, **kwargs):
    super(HexInput, self).__init__(**kwargs)
    self.multiline = False


# A representation in decimal and hexadecimal
class IntInput(BoxLayout):
  val = StringProperty()
  
  def __init__(self, text="", **kwargs):
    super(IntInput, self).__init__(**kwargs)
    self.val = text

  # Automagically called when val is modified
  def on_val(self, instance, value):
    # Case data
    if(isInt(value, base=10)):
      self.dec.text = value
      self.hex.text = "%#x" % int(value, 10)

    # Case instr
    else:
      self.dec.text = value
      self.hex.text = value

  def changeval(self, value):
    if(isInt(value, base=10)):
      self.val = str(int(value, 10))
    elif(isInt(value, base=16)):
      self.val = str(int(value, 16))
    else:
      self.val = value


def isInt(s, base=10):
  try: 
    int(s, base)
    return True
  except ValueError:
    return False


class Zemulator(App):
  def __init__(self, emu=Z33(), file= "", **kwargs):
    super(Zemulator, self).__init__(**kwargs)
    self.emu = emu
    self.filename = file

  def update(self):
    self.mw.update()

  def build(self):
    Builder.load_string(KV)
    self.title = 'Zorglub33 emulator'
    self.mw = MainWindow(emu=self.emu, file=self.filename)
    return self.mw
